package com.sda.javaZadaniaDomowe.streamy;

/**
 Zadania ze streamów do przećwiczenia:
 1. Utwórzcie sobie listę 20 obiektów typu string - wpiszcie tam tytuły filmów
 2. Za pomocą streamów wyciągnijcie z tej listy:
 a) Listę tytułów zaczynających się na literę A
 b) Listę tytułów, które mają dwa słowa w nazwie
 c) listę tytułów, które mają więcej niż 5 znaków
 d) Tytuł o najdłuższej nazwie
 e) Informację postaci: ile tytułów składa się z jednego wyrazu, ile tytułów składa się z dwóch wyrazów itp.
 */
public class TytulFilmu {
}
