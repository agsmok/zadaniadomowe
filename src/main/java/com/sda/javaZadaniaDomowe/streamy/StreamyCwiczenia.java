package com.sda.javaZadaniaDomowe.streamy;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StreamyCwiczenia {

    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            lista.add(i + 1);
        }

        //wszystkie liczby podzielne przez dwa
        List<Integer> podzielnePrzez2 = lista.stream().filter(new Predicate<Integer>() {
            @Override
            public boolean test(Integer integer) {
                return integer % 2 == 0;
            }
        }).collect(Collectors.toList());

        List<Integer> podzielneprzezDwaPlusLambda = lista.stream().filter(liczba -> liczba % 2 == 0).collect(Collectors.toList());

        List<Integer> podzielnePrzezTrzyiWiekszeOd20 = lista.stream().filter(liczba -> liczba % 3 == 0 && liczba > 20).collect(Collectors.toList());
// wszystkie liczby z przedziału 0 do 50 ktore sa podzielne przez dwa a jes=zeli sa z przedziału powyzej sprawdzamy czy sa podzielne przez 3
        List<Integer> wyniki1 = lista.stream().filter(liczba -> {
            if (liczba < 51) {
                return liczba % 2 == 0;
            } else {
                return liczba % 3 == 0;
            }
        }).collect(Collectors.toList());

        //lista ktora zwroci liczby podzielne przez 2 i 3  z przedziału od 20 do 40 a jezeli są wieksze od 40 to mają być podzielne przez
        //  0-20 podzielne przez 3 i 7
        //  20-40   2 i 3
        //  40-100 4 i 6
        List<Integer> wyniki2 = lista.stream().filter(liczba -> {
            if (liczba < 20) {
                return (liczba % 3 == 0 && liczba % 7 == 0);
            } else if (liczba > 20 && liczba < 40) {
                return liczba % 2 == 0 && liczba % 3 == 0;
            } else if (liczba > 40 && liczba < 100) {
                return liczba % 4 == 0 && liczba % 6 == 0;
            } else {
                return false;
            }
        }).collect(Collectors.toList());

        //dla kazdej liczby chcemy zwrocic kolekcje o 3 razy wieksza

        List<Integer> wieksze3Razy = lista.stream().map(liczba -> liczba * 3).collect(Collectors.toList());
//
//
// lista obiektow
// <0 - 20 > *3 + 2
// < 21 - 40 >  *5 - 50
// < 41 100 > ^2

        List<Integer> wynik3 = lista.stream().map(liczba -> {
            if (liczba <= 20) {
                return liczba * 3 + 2;
            } else if (liczba > 20 && liczba <= 40) {
                return liczba * 5 - 50;
            } else if (liczba > 40 && liczba <= 100) {
                return liczba * liczba;
            } else {
                return liczba;
            }
        }).collect(Collectors.toList());

        // lista ktora zamieni liste i zawiera wartosci do wartosci ktore są wielokrotnoscia 4

        List<Integer> wynik4 = lista.stream().map(liczba -> {
            if (liczba <= 20) {
                return liczba * 3 + 2;
            } else if (liczba > 20 && liczba <= 40) {
                return liczba * 5 - 50;
            } else if (liczba > 40 && liczba <= 100) {
                return liczba * liczba;
            } else {
                return liczba;
            }
        }).filter(liczba -> liczba % 4 == 0).collect(Collectors.toList());

        Random random = new Random();
        //lista poczatkowa od 1 do 100 przemnozyc przez randon,next int(100)
        //znaleźź najwieksza liczbą max

        Integer max = lista.stream().mapToInt(liczba -> liczba * random.nextInt(100)).max().getAsInt();

        System.out.println(max);

        OptionalInt max2 = lista.stream().mapToInt(liczba -> liczba).filter(liczba -> liczba > 1000).max();
        if (max2.isPresent()) {
            System.out.println(max2.getAsInt());
        }
        System.out.println("Zbiór jest pusty");

        System.out.println(max2);

        // randon i zwraca średnią
        double srednia = lista.stream().mapToInt(liczba -> liczba * random.nextInt(100)).average().getAsDouble();
        System.out.println(srednia);


        // randon i znaleźć pierwszą liczbę w tym zbiorze która jest podzielna przez dwa
        Optional<Integer> pierwszaPodzielnaPrzezDwa = lista.stream().map(liczba ->liczba*random.nextInt(100)).filter(liczba -> liczba % 2 == 0).findFirst();
        if (pierwszaPodzielnaPrzezDwa.isPresent()){
            System.out.println(pierwszaPodzielnaPrzezDwa.get());
        } else {System.out.println("nie znaleziono wynikow");}

//

//        System.out.println(podzielnePrzez2);
//        System.out.println("========================");
//        System.out.println(podzielneprzezDwaPlusLambda);
//        System.out.println("========================");
//        System.out.println(podzielnePrzezTrzyiWiekszeOd20);
//        System.out.println("========================");
//        System.out.println(wyniki1);
//        System.out.println("========================");
//        System.out.println(lista);
//        System.out.println("========================");
//        System.out.println(wyniki2);
//        System.out.println(wyniki2);
//        System.out.println(wieksze3Razy);
//        System.out.println(wynik3);
//        System.out.println("========================");
//        System.out.println(wynik4);

    }

}
