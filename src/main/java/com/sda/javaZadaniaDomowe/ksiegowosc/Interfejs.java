package com.sda.javaZadaniaDomowe.ksiegowosc;

import java.util.*;
import java.util.stream.Collectors;

public class Interfejs {
    public static void main(String[] args) {
        String sciezkaDoPliku = "C:/Users/Admin/IdeaProjects/Baza/bazaZarobki.csv";

        List<Map<String, Object>> wierszePliku = NarzedziaCSV.wczytajCsvZPliku(sciezkaDoPliku);
        //System.out.println(wierszePliku.get(0));
        List<Persona> osoby = NarzedziaCSV.zamienMapeNaPresone(wierszePliku);
        //System.out.println(osoby.get(0));

        //Wszystkie osoby o imieniu "..."
        List<Persona> adamowie = osoby.stream().filter(persona -> persona.getImie().equals("Dariusz"))
                .collect(Collectors.toList());
        // wyswietlListe(adamowie);
        //   System.out.println(adamowie.size());

        //Pobieranie listy nazwisk
        List<String> nazwiska = osoby.stream().map(persona -> persona.getNazwisko()).collect(Collectors.toList());
        //System.out.println(nazwiska);
        Set<String> nazwiskaUnikalne = osoby.stream().map(persona -> persona.getNazwisko()).collect(Collectors.toSet());
        //System.out.println(nazwiskaUnikalne);
        //osoby starsze niz 35 lat i nazwiska zaczynające się na literę S

        List<Persona> starszeNiz35INazwisnaS = osoby.stream().filter(persona -> persona.getWiek() > 35
                && persona.getNazwisko().charAt(0) == 'S').collect(Collectors.toList());
        // wyswietlListe(starszeNiz35INazwisnaS);


        //Osoby w wieku produkcyjnym

        List<Persona> osobyWWiekuProdukcyjnym = osoby.stream().filter(persona -> {
            if (persona.getPlec().equals(Plec.KOBIETA)) {
                return persona.getWiek() > 18 && persona.getWiek() < 59;
            } else {
                return persona.getWiek() > 18 && persona.getWiek() < 64;
            }
        }).collect(Collectors.toList());
        //wyswietlListe(osobyWWiekuProdukcyjnym);

        //Osoby mieszkające w Gdańsu lub Warszawie

        List<Persona> osobyZGdanskaIWarszay = osoby.stream().filter(persona -> persona.getMiasto().equals("Warszawa") ||
                persona.getMiasto().equals("Gdańsk")).collect(Collectors.toList());
        //wyswietlListe(osobyZGdanskaIWarszay);

        //osoby urodzone w maju
        List<Persona> osobyUrodzoneWMaju = osoby.stream().filter(persona -> persona.getDataUrodzenia().getMonthValue() == 05)
                .collect(Collectors.toList());
        //wyswietlListe(osobyUrodzoneWMaju);


        //osoby których kod pocztowy zaczyna się od cyfry 9

        List<Persona> osobyZKodemPocztowym9 = osoby.stream().filter(persona -> persona.getKodPocztowy().substring(0, 1).equals("9"))
                .collect(Collectors.toList());
        // wyswietlListe(osobyZKodemPocztowym9);

        //lista unikalnych zawodów

        Set<String> unikalneZawody = osoby.stream().map(persona -> persona.getZawod()).collect(Collectors.toSet());
        // System.out.println(unikalneZawody);
        //System.out.println(unikalneZawody.size());


        //wyswietl pierwsze 20 0sob z najwyzszymi zarobkami

        List<Persona> osobyZNajwyzszymiZarobkami = osoby.stream().sorted((o1, o2) -> o2.getZarobki().compareTo(o1.getZarobki())).limit(20)
                .collect(Collectors.toList());
        //  wyswietlListe(osobyZNajwyzszymiZarobkami);
        //innym sposobem
        System.out.println("=======================");

        najwyzsze20Dochodow(osoby);

        //lista osob ktorych nazwisko zaczyna się na C

        List<Persona> osobyZNazwiskiemNaC = osoby.stream().filter(persona -> persona.getNazwisko().charAt(0) == 'C')
                .collect(Collectors.toList());
        //wyswietlListe(osobyZNazwiskiemNaC);

        // wyswietlic srednią zarobków dla osób w przedziale 21-30

        Double sredniaZarobkow = osoby.stream().filter(persona -> persona.getWiek() > 21 && persona.getWiek() < 30)
                .mapToInt(persona -> persona.getZarobki()).average().getAsDouble();
        System.out.println("średnia zarobków dla osób w przedziale 21-30 wynosi: " + sredniaZarobkow);

        Double redniezarobkiwszystkich=osoby.stream().mapToInt(persona->persona.getZarobki()).average().getAsDouble();
        System.out.println("redniezarobkiwszystkich"+redniezarobkiwszystkich);

        //wyswietlić ilość kobiet i mężczyzn w zestawieniu

        Long ilośćKobiet = osoby.stream().filter(persona -> persona.getPlec().equals(Plec.KOBIETA)).count();
        Long ilośćMezczyzn = osoby.stream().filter(persona -> persona.getPlec().equals(Plec.MEZCZYZNA)).count();

        System.out.println(ilośćKobiet + ilośćMezczyzn + " osób w tym " + "Liczba Kobiet: " +
                ilośćKobiet + " Liczba mężczyzn: " + ilośćMezczyzn);

        //wyswietlić ilość kobiet i mężczyzn w zestawieniu 2 sposob
        int[] wynik = osoby.stream().map(persona -> {
            if (persona.getPlec().equals(Plec.KOBIETA)) {
                return new int[]{1, 0};
            } else {
                return new int[]{0, 1};
            }
        }).reduce(new int[2], (a, b) -> {
            a[0] = a[0] + b[0]; //ilosc kobiet
            a[1] = a[1] + b[1];  //ilosc mezczyzn
            return a;
        });
        System.out.println(wynik[0] + " " + wynik[1]);


        //dla kazdego stanowiska wyswietlic średnia zarobków




    }

    private static void najwyzsze20Dochodow(List<Persona> osoby) {
        List<Persona> copy = new ArrayList<>(osoby);
        Collections.sort(copy, new Comparator<Persona>() {
            @Override
            public int compare(Persona o1, Persona o2) {
                return o2.getZarobki().compareTo(o1.getZarobki());
            }
        });
        List<Persona> osobyZNajwyzszymiZarobkamiNowaMetoda = copy.subList(0, 20);
        wyswietlListe(osobyZNajwyzszymiZarobkamiNowaMetoda);
    }



    private static void wyswietlListe(List<Persona> lista) {
        for (Persona persona : lista) {
            System.out.println(persona);
        }

    }
    //wyswietlić ilość osób pracujących w danym miescie


    //wyswietlić liste osób pracujących w danym miescie

//    private static void dajListeOsobWgMiast(List<Persona>osoba,String miasto){
//        List<Persona>lista = osoba.stream().filter(persona ->{
//            for (persona.getMiasto().equals(miasto))
//                return  miasto
//                }
//
//        for ()
//
//        }
}
