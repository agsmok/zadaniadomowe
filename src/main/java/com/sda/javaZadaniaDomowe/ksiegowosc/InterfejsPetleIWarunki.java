package com.sda.javaZadaniaDomowe.ksiegowosc;

import java.util.*;

public class InterfejsPetleIWarunki {
    public static void main(String[] args) {
        String sciezkaDoPliku = "C:/Users/Admin/IdeaProjects/Baza/bazaZarobki.csv";

        List <Map <String, Object>> wierszePliku = NarzedziaCSV.wczytajCsvZPliku(sciezkaDoPliku);
//System.out.println(wierszePliku.get(0));
        List <Persona> osoby = NarzedziaCSV.zamienMapeNaPresone(wierszePliku);
//        System.out.println(osoby.get(0));
//        System.out.println(osoby.size());
//        wyswietlDariuszy(osoby);
//        pobierzNazwiska(osoby);
//        pobieranieUnikalnychNazwisk(osoby);//????????????????????????????????????????????????????
//        starszeNiz35INazwisnaS(osoby);
//        osobyWWiekuProdukcyjnym(osoby);
//        osobyZGdanskaIWarszay(osoby);
//        wyswietlOsobyUrodzoneWMaju(osoby);
//        wyswietlOsobyZKodemPocztowymZaczynajacymSieNa9(osoby);
//        wyswietlOsobyZNazwiskiemNaC(osoby);
//        System.out.println("średnia " + wyswietlSredniaZarobkow(osoby));
//        int iloscOsobwgPłci[] = iloscKobietIIloscMezczyzn(osoby);
//        int iloscKobiet = iloscOsobwgPłci[0];
//        int iloscMezczyzn = iloscOsobwgPłci[1];
//        System.out.println("iloscKobiet " + iloscKobiet + " iloscMezczyzn " + iloscMezczyzn);
//        String miasto = "Warszawa";
//        int iloscOsob = wyswietlIloscOsobPracujacychWDanymMiescie(osoby, miasto);
//        System.out.println("W mieście " + miasto + " pracuje " + iloscOsob + " osób");
//        wyswietlListeOsobPracujacychWDanymMiescie(osoby, miasto);
//        pobieranieUnikalnychZawodow(osoby);
//        wyswietlSredniaZarobkowWgStanowiska(osoby);

    }

    private static void wyswietlDariuszy(List <Persona> personaList) {
        List <Persona> dariusz = new ArrayList <>();
        for (Persona persona : personaList) {
            if (persona.getImie().equals("Dariusz")) {
                dariusz.add(persona);
            }
            wyswietlListe(dariusz);
        }
    }

    //Pobieranie listy nazwisk

    private static void pobierzNazwiska(List <Persona> personaList) {
        List <String> listaNazwisk = new ArrayList <>();
        for (Persona persona : personaList) {
            listaNazwisk.add(persona.getNazwisko());
            System.out.println(listaNazwisk);
        }
    }

    private static void pobieranieUnikalnychNazwisk(List <Persona> personaList) {
        Set <String> unikalnaListaNazwisk = new HashSet <>();
        for (Persona persona : personaList) {
            unikalnaListaNazwisk.add(persona.getNazwisko());
        }
        for (String nazwisko : unikalnaListaNazwisk) {
            System.out.println(nazwisko);
        }
    }

    //    osoby starsze niz 35 lat i nazwiska zaczynające się na literę S
    private static void starszeNiz35INazwisnaS(List <Persona> personaList) {
        List <Persona> lista = new ArrayList <>();
        for (Persona persona : personaList) {
            if (persona.getWiek() > 35 && (persona.getNazwisko().charAt(0) == 'S')) {
                lista.add(persona);
            }
        }
        wyswietlListe(lista);
    }

    //        Osoby w wieku produkcyjnym
    private static void osobyWWiekuProdukcyjnym(List <Persona> personaList) {
        final List <Persona> osobyWWiekuProdukcyjnym = new ArrayList <>();
        for (Persona persona : personaList) {
            if (persona.getPlec().equals(Plec.KOBIETA) && persona.getWiek() > 18 && persona.getWiek() < 59) {
                osobyWWiekuProdukcyjnym.add(persona);
            } else if (persona.getWiek() > 18 && persona.getWiek() < 64) {
                osobyWWiekuProdukcyjnym.add(persona);
            }
        }
        wyswietlListe(osobyWWiekuProdukcyjnym);
    }


    //Osoby mieszkające w Gdańsu lub Warszawie
    private static void osobyZGdanskaIWarszay(List <Persona> personaList) {
        List <Persona> listaosobZGdanskaIWarszay = new ArrayList <>();
        for (Persona persona : personaList) {
            if (persona.getMiasto().equals("Warszawa") || persona.getMiasto().equals("Gdańsk")) {
                listaosobZGdanskaIWarszay.add(persona);
            }
        }
        wyswietlListe(listaosobZGdanskaIWarszay);
    }

    //osoby urodzone w maju
    private static void wyswietlOsobyUrodzoneWMaju(List <Persona> personaList) {
        List <Persona> osobyUrodzoneWMaju = new ArrayList <>();
        for (Persona persona : personaList) {
            if (persona.getDataUrodzenia().getMonthValue() == 05) {
                osobyUrodzoneWMaju.add(persona);
            }
        }
        wyswietlListe(osobyUrodzoneWMaju);
    }

    //osoby których kod pocztowy zaczyna się od cyfry 9
    private static void wyswietlOsobyZKodemPocztowymZaczynajacymSieNa9(List <Persona> personaList) {
        List <Persona> osobyZKodemPocztowymOd9 = new ArrayList <>();
        for (Persona persona : personaList) {
            if (persona.getKodPocztowy().charAt(0) == '9') {
                osobyZKodemPocztowymOd9.add(persona);
            }
        }
        wyswietlListe(osobyZKodemPocztowymOd9);
    }

    //        //lista unikalnych zawodów
    private static void pobieranieUnikalnychZawodow(List <Persona> personaList) {
        Set <String> unikalnaListaZawodow = new HashSet <>();
        for (Persona persona : personaList) {
            unikalnaListaZawodow.add(persona.getZawod());
        }
        for (String zawod : unikalnaListaZawodow) {
            System.out.println(zawod);
        }
    }

    //      lista osob ktorych nazwisko zaczyna się na C
    private static void wyswietlOsobyZNazwiskiemNaC(List <Persona> personaList) {
        List <Persona> osobyZNazwiskiemNaC = new ArrayList <>();
        for (Persona persona : personaList) {
            if (persona.getNazwisko().charAt(0) == 'C') {
                osobyZNazwiskiemNaC.add(persona);
            }
        }
        wyswietlListe(osobyZNazwiskiemNaC);
    }

    //     wyswietlic srednią zarobków dla osób w przedziale 21-30
    private static double wyswietlSredniaZarobkow(final List <Persona> personaList) {
        double suma = 0;
        final List <Integer> zarobki = new ArrayList <>();
        for (final Persona persona : personaList) {
            if (persona.getWiek() <= 21 || persona.getWiek() >= 30) {
                continue;
            }
            zarobki.add(persona.getZarobki());
            suma += persona.getZarobki();
        }
        return suma / zarobki.size();
    }

    //      wyswietlić ilość kobiet i mężczyzn w zestawieniu
    private static int[] iloscKobietIIloscMezczyzn(List <Persona> personaList) {
        int[] iloscOsobWgPlci = new int[]{0, 0};
        for (Persona persona : personaList) {
            if (persona.getPlec().equals(Plec.KOBIETA)) {
                iloscOsobWgPlci[0] = iloscOsobWgPlci[0] + 1;
            } else iloscOsobWgPlci[1] = iloscOsobWgPlci[1] + 1;
        }
        return iloscOsobWgPlci;
    }

//      dla kazdego stanowiska wyswietlic średnia zarobków

    private static Map <String, Double> wyswietlSredniaZarobkowWgStanowiska(List <Persona> personaList){
        pobieranieUnikalnychZawodow(personaList);
        return null;
    }

//

    //wyswietlić liste osób pracujących w danym miescie
    private static void wyswietlListeOsobPracujacychWDanymMiescie(List <Persona> personaList, String miasto) {
        List <Persona> listaOsobPracujacychWMiescie = new ArrayList <>();
        for (Persona persona : personaList) {
            if (persona.getZarobki() == null || !persona.getMiasto().equals(miasto)) {
                continue;
            }
            listaOsobPracujacychWMiescie.add(persona);

        }
        wyswietlListe(listaOsobPracujacychWMiescie);
    }


    //wyswietlić ilość osób pracujących w danym miescie
    private static int wyswietlIloscOsobPracujacychWDanymMiescie(List <Persona> personaList, String miasto) {
        int liczbaOsobPracujacaWMiescie = 0;
        for (Persona persona : personaList) {
            if (persona.getZarobki() == null || !persona.getMiasto().equals(miasto)) {
                continue;
            }
            liczbaOsobPracujacaWMiescie += 1;
        }
        return liczbaOsobPracujacaWMiescie;
    }

    private static void wyswietlListe(List <Persona> lista) {
        for (Persona persona : lista) {
            System.out.println(persona);
        }
    }
}
