package com.sda.javaZadaniaDomowe.spoj;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Kalkulator {
    /**
     * CALC - Kalkulator
     * Napisz program, który działa jak prosty kalkulator obsługujący pięć operacji: dodawanie, odejmowanie, mnożenie, dzielenie i obliczanie reszty z dzielenia liczb całkowitych.
     * <p>
     * Wejście
     * Na wejście programu podana zostanie pewna nieokreślona liczba zestawów danych. Zestawy składają się z jednoznakowego symbolu operacji do wykonania (+ dodawanie, - odejmowanie, * mnożenie, / dzielenie całkowitoliczbowe, % reszta z dzielenia) oraz następujących po nim dwóch liczb całkowitych. Poszczególne składowe zestawu zostaną rozdzielone spacjami, a same zestawy znakiem nowej linii. Liczba testów nie przekracza 100, wynik zawiera się w typie int32.
     * <p>
     * Wyjście
     * Na wyjściu programu ma się pojawić ciąg liczb będących rezultatem wykonania pojawiających się na wejściu poleceń. Poszczególne liczby należy rozdzielić znakami nowej linii. Uwaga! Można założyć, że dane wejściowe nie zawierają polecenia dzielenia przez 0.
     * <p>
     * Przykład
     * Wejście:
     * + 7 9
     * - 0 4
     * 5 6
     * / 8 3
     * % 5 2
     * <p>
     * Wyjście:
     * 16
     * -4
     * 30
     * 2
     * 1
     */

    public static void main(String[] args) {
        Random random = new Random();
        int liczbaTestow = random.nextInt(100);
        System.out.println("liczbaTestow " + liczbaTestow);

        List<String> listaDziałan = new ArrayList<String>();
        List<Double> listaWynikow = new ArrayList<Double>();

        String[] znak = new String[]{"+", "-", "*", "/", "%"};

        for (int i = 0; i < liczbaTestow; i++) {

            int liczba1 = random.nextInt(100);
            int liczba2 = random.nextInt(100);
            String losowyZnak = znak[random.nextInt(5)];

            if (liczba2 == 0 && (losowyZnak.equals("/") || losowyZnak.equals("%"))) {
                do {
                    liczba2 = random.nextInt(100);
                } while (liczba2 == 0);
            }

            String działanie = losowyZnak + " " + liczba1 + " " + liczba2;
            listaDziałan.add(działanie);

            double wynik;

            switch (losowyZnak) {
                case "+":
                    wynik = liczba1 + liczba2;
                    break;
                case "-":
                    wynik = liczba1 + liczba2;
                    break;
                case "*":
                    wynik = liczba1 * liczba2;
                    break;
                case "/":
                    wynik = ((double) liczba1 / (double) liczba2);
                    break;
                case "%":
                    wynik = liczba1 % liczba2;
                    break;
                default:
                    wynik = 0;
            }
            listaWynikow.add(wynik);
        }
        for (String dzialanie : listaDziałan) {
            System.out.println(dzialanie);
        }
        System.out.println();
        for (double wynik : listaWynikow) {
            System.out.println(wynik);

        }


    }
}
