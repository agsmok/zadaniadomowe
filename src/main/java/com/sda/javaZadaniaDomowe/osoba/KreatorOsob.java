package com.sda.javaZadaniaDomowe.osoba;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class KreatorOsob {
    private List<Osoba> listaOsob;

    public KreatorOsob() {
        this.listaOsob = new ArrayList<>();
    }

    public void setListaOsob(List<Osoba> listaOsob) {
        this.listaOsob = listaOsob;
    }

    public void dodajOsobe(Osoba osoba) {
        if (osoba != null)
            listaOsob.add(osoba);

    }

    public List<String> pokazListeOsob() {
        List<String> wynik = new ArrayList<>();
        for (Osoba osoba : listaOsob) {
            String opis;
            opis = String.format(osoba.toString());
            wynik.add(opis);
        }
        return wynik;
    }

//    public Queue<Osoba> zwrocListeOsobWgKryterium(KryteriumSortowania pierwszeKryterium, KryteriumSortowania drugieKryterium) {
//        PorownywanieOsob porownywanieOsob = new PorownywanieOsob(pierwszeKryterium, drugieKryterium);
//        Queue<Osoba> wynik = new PriorityQueue<>(porownywanieOsob);
//        wynik.addAll(listaOsob);
//        return wynik;
//    }
}
