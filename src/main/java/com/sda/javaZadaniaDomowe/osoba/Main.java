package com.sda.javaZadaniaDomowe.osoba;

import java.time.LocalDate;
import java.util.Queue;

public class Main {
    public static void main(String[] args) {
        KreatorOsob kreatorOsob = new KreatorOsob();
        dodajOsobeDoListy(kreatorOsob);
//        System.out.println(kreatorOsob.pokazListeOsob());

//        Queue <Osoba> osobaWgWieku = kreatorOsob.zwrocListeOsobWgKryterium(KryteriumSortowania.WIEK, KryteriumSortowania.NAZWISKO);
//        while (!osobaWgWieku.isEmpty()) {
//            System.out.println(osobaWgWieku.poll());
//        }
    }


    private static void dodajOsobeDoListy(KreatorOsob kreatorOsob) {
        kreatorOsob.dodajOsobe(new Osoba("Agnieszka", "Smok", "Ksawerow", "95-054", "Rzepakowa", 30, LocalDate.of(1987, 06, 12), "programista", Plec.KOBIETA));
        kreatorOsob.dodajOsobe(new Osoba("Anna", "Wisniewska", "Ksawwerów", "95-084", "Rzepakowa", 33, LocalDate.of(1983, 11, 12), "urzędnik", Plec.KOBIETA));
        kreatorOsob.dodajOsobe(new Osoba("Magdalena", "Walaszczyk", "Pabianice", "95-200", "Jałowcowa", 32, LocalDate.of(1985, 10, 27), "ekonomista", Plec.KOBIETA));
        kreatorOsob.dodajOsobe(new Osoba("Dorota", "Wawrzenczak", "Warszawa", "92-001", "Kondratowicza", 45, LocalDate.of(1975, 2, 28), "pracownik biurowy", Plec.KOBIETA));
        kreatorOsob.dodajOsobe(new Osoba("Przemysław", "Zglinicki", "Ksawerów", "95-054", "Wschodnia", 25, LocalDate.of(1997, 2, 10), "programista", Plec.MEZCZYZNA));
        kreatorOsob.dodajOsobe(new Osoba("Marcin", "Zglinicki", "Pabianice", "95-200", "Dolna", 27, LocalDate.of(1990, 5, 30), "programista", Plec.MEZCZYZNA));
        kreatorOsob.dodajOsobe(new Osoba("Agata", "Szrajber", "Łódź", "97-154", "Jałowcowa", 30, LocalDate.of(1987, 8, 25), "specjalita", Plec.KOBIETA));
        kreatorOsob.dodajOsobe(new Osoba("Krzysztof", "Matyka", "Łódź", "97-154", "Jałowcowa", 31, LocalDate.of(1986, 4, 25), "informatyk", Plec.MEZCZYZNA));
        kreatorOsob.dodajOsobe(new Osoba("Elzbieta", "Staszewska", "Krakow", "54-756", "Piękna", 33, LocalDate.of(1975, 6, 24), "księgowa", Plec.KOBIETA));

    }


}
