package com.sda.javaZadaniaDomowe.osoba;

import java.time.LocalDate;

/**
 * a) Utwórz klasę Osoba składającą się z następujących następujących:
 * imie, nazwisko, miasto, kodPocztowy, ulica, wiek (liczba całkowita), dataUrodzenia (localDate), zawod, plec (enum)
 * konstruktor, gettery + settery, toString ()
 * b) Utworzenia 10 obiektów klasy Osoba i zapisz je na liście
 * c) Zaimplementuj interfejs Porównywalny - metoda compareTo ma porówn imię i i
 * datę urodzenia d) Metoda wpisywania się, która z innymi osobami starsze niż X lat (X podraże w parametrach metody)
 */

public class Osoba{
   private String imie;
    private String nazwisko;
    private String miasto;
   private String kodPocztowy;
    private String ulica;
    int wiek;
    LocalDate dataUrodzenia;
    private String zawod;

    Plec plec;

    public Osoba(String imie, String nazwisko, String miasto, String kodPocztowy, String ulica, int wiek, LocalDate dataUrodzenia, String zawod, Plec plec) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.miasto = miasto;
        this.kodPocztowy = kodPocztowy;
        this.ulica = ulica;
        this.wiek = wiek;
        this.dataUrodzenia = dataUrodzenia;
        this.zawod = zawod;
        this.plec = plec;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getMiasto() {
        return miasto;
    }

    public void setMiasto(String miasto) {
        this.miasto = miasto;
    }

    public String getKodPocztowy() {
        return kodPocztowy;
    }

    public void setKodPocztowy(String kodPocztowy) {
        this.kodPocztowy = kodPocztowy;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public void setDataUrodzenia(LocalDate dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;
    }

    public String getZawod() {
        return zawod;
    }

    public void setZawod(String zawod) {
        this.zawod = zawod;
    }

    public Plec getPlec() {
        return plec;
    }

    public void setPlec(Plec plec) {
        this.plec = plec;
    }

    @Override
    public java.lang.String toString() {
        return "Osoba{" + "imie=" + imie + ", nazwisko=" + nazwisko + ", miasto=" + miasto + ", kodPocztowy=" + kodPocztowy + ", ulica=" + ulica + ", wiek=" + wiek + ", dataUrodzenia=" + dataUrodzenia + ", zawod=" + zawod + ", plec=" + plec + '}';
    }

}
