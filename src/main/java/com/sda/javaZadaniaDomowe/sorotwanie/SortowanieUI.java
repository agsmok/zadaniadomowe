package com.sda.javaZadaniaDomowe.sorotwanie;

import com.sda.javaZadaniaDomowe.sorotwanie.sortowaniebabelkowe.SortowanieBabelkowe;
import com.sda.javaZadaniaDomowe.sorotwanie.sortowanieprzezwstawianie.SortowaniePrzezWstawianie;
import com.sda.javaZadaniaDomowe.sorotwanie.sortowanieprzezwybor.SortowaniePrzezWybor;
import com.sda.javaZadaniaDomowe.sorotwanie.sortowanieprzezzliczanie.SortowaniePrzezZliczanie;
import com.sda.javaZadaniaDomowe.sorotwanie.sortowanieszybkie.SortowanieSzybkie;

public class SortowanieUI {
    public static void main(String[] args) {
        int[] tablica = new int[]{2, 8, 5, 9, 6};

       Sortowanie[]sortowanie = new Sortowanie[5];
       sortowanie[0] = new SortowanieBabelkowe();
       sortowanie[1] = new SortowaniePrzezWstawianie();
       sortowanie[2] = new SortowaniePrzezZliczanie();
       sortowanie[3]= new SortowaniePrzezWybor();
       sortowanie[4]=new SortowaniePrzezWybor();


        wyswietlTablice(tablica);

//        Sortowanie sortowanie = new SortowanieBabelkowe();
//        Sortowanie sortowanie = new SortowaniePrzezWybor();
//        Sortowanie sortowanie = new SortowaniePrzezZliczanie();
//        Sortowanie sortowanie = new SortowaniePrzezWstawianie();
//        Sortowanie sortowanie = new SortowanieSzybkie();

//        tablica = sortowanie.sortuj(tablica, Porzadek.ROSNACY);
wyswietlTablice(tablica);
    }
    public static void wyswietlTablice(int[]tablica) {
        for (int elem : tablica) {
            System.out.print(elem + " ");
        }
    }
}
