package com.sda.javaZadaniaDomowe.sorotwanie.sortowanieprzezzliczanie;

import com.sda.javaZadaniaDomowe.sorotwanie.Porzadek;
import com.sda.javaZadaniaDomowe.sorotwanie.Sortowanie;

import java.util.ArrayList;
import java.util.List;

public class SortowaniePrzezZliczanie implements Sortowanie {
    @Override
    public int[] sortuj(int[] tabliceElementow, Porzadek porzadek) {
        int max = 0;
        for (int i = 0; i < tabliceElementow.length; i++) {
            if (tabliceElementow[i] > max) {
                max = tabliceElementow[i];
            }
        }
        int[] zliczanieElementow = new int[max + 1];
        for (int i = 0; i < tabliceElementow.length; i++) {
            int liczba = tabliceElementow[i];
            zliczanieElementow[liczba] += 1;
        }

        List <Integer> list = new ArrayList <>();
        for (int i = 0; i < zliczanieElementow.length; i++) {
            for (int j = 0; j < zliczanieElementow[i]; j++) {
                list.add(i);
            }
        }

        for (int i = 0; i < list.size(); i++) {
            tabliceElementow[i] = list.get(i);
        }
        System.out.println();
        return tabliceElementow;
    }
}

//    int[] tablica = new int[]{5, 3, 8, 1, 1, 3, 8, 6, 4, 5};
//        int[] zliczenia = new int[10];
//        List <Integer> list = new ArrayList <>();
//
//
//        for (int i = 0; i < tablica.length; i++) {
//            zliczenia[tablica[i]]++;
//        }
//
//        for (int i = 0; i < zliczenia.length; i++) {
//            if (zliczenia[i] > 0) {
//                for (int j = 1; j < zliczenia[i] + 1; j++) {
//                    list.add(i);
//
//                }
//            }
//
//
//        }
//        System.out.println(list);