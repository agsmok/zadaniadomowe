package com.sda.javaZadaniaDomowe.sorotwanie.sortowanieprzezwybor;

import com.sda.javaZadaniaDomowe.sorotwanie.Porzadek;
import com.sda.javaZadaniaDomowe.sorotwanie.Sortowanie;
import com.sda.javaZadaniaDomowe.sorotwanie.SortowanieUI;

public class SortowaniePrzezWybor implements Sortowanie {
    @Override
    public int[] sortuj(int[] tabliceElementow, Porzadek porzadek) {
        for (int i=0;i<tabliceElementow.length;i++){
            int min = tabliceElementow[i];
            int indexMinimum = i;
            for (int j=i+1; j<tabliceElementow.length;j++){
                if (tabliceElementow[j]<min){
                    min=tabliceElementow[j];
                    indexMinimum=j;
                }
            }
            //zamieniamy miejscami minimum
            tabliceElementow[indexMinimum]=tabliceElementow[i];
            tabliceElementow[i]=min;


        }
        System.out.println();
        return tabliceElementow;
    }
}
