package com.sda.javaZadaniaDomowe.sorotwanie.sortowaniebabelkowe;

import com.sda.javaZadaniaDomowe.sorotwanie.Sortowanie;
import com.sda.javaZadaniaDomowe.sorotwanie.Porzadek;
import com.sda.javaZadaniaDomowe.sorotwanie.SortowanieUI;

public class SortowanieBabelkowe implements Sortowanie {
    public int[] sortuj(int[] tabliceElementow, Porzadek porzadek) {
        int iloscoperacji = 0;
        for (int i = 0; i < tabliceElementow.length; i++) {
            System.out.println("Biorę i-ty element tablicy: " + i + " jest to liczba: " + tabliceElementow[i]);
            for (int j = i + 1; j < tabliceElementow.length; j++) {
                System.out.println("Biorę j-ty element tablicy: " + j + " jest to liczba: " + tabliceElementow[j]);
                iloscoperacji++;
                if (i != j) {  //nie chemy porownywac tego samego elementu
                    //czy element o indeksie 0>od elementu o indeksie 1 itd
                    switch (porzadek) {
                        case ROSNACY:
                            System.out.println(String.format("Porównuję czy liczba %d<%d", tabliceElementow[j], tabliceElementow[i]));
                            if (tabliceElementow[j] < tabliceElementow[i]) {
                                System.out.println("Zamieniam miejscami liczby");
                                System.out.println(String.format("Indeksy i i j wskazuja na liczby %d, %d", tabliceElementow[i], tabliceElementow[j]));
                                //zamien miejscami wartosci tablicy
                                int tmp = tabliceElementow[i];
                                tabliceElementow[i] = tabliceElementow[j];
                                tabliceElementow[j] = tmp;
                                System.out.println(String.format("Indeksy i i j wskazuja na liczby %d, %d", tabliceElementow[i], tabliceElementow[j]));

                            }
                            break;
                        case MALEJACY:
                            if (tabliceElementow[i] > tabliceElementow[j]) {
                                //zamien miejscami wartosci tablicy
                                int tmp = tabliceElementow[i];
                                tabliceElementow[i] = tabliceElementow[j];
                                tabliceElementow[j] = tmp;
                            }
                            break;
                    }
                }
            }
            SortowanieUI.wyswietlTablice(tabliceElementow);

        }
        System.out.println();
        System.out.println("ilosc operacji " + iloscoperacji);
        return tabliceElementow;
    }
}
