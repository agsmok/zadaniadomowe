package com.sda.javaZadaniaDomowe.sorotwanie.sortowanieprzezwstawianie;

import com.sda.javaZadaniaDomowe.sorotwanie.Porzadek;
import com.sda.javaZadaniaDomowe.sorotwanie.Sortowanie;

import java.util.ArrayList;
import java.util.List;

public class SortowaniePrzezWstawianie implements Sortowanie {
    @Override
    public int[] sortuj(int[] tabliceElementow, Porzadek porzadek) {
        List <Integer> wynik = new ArrayList <>();
        for (int i = 0; i < tabliceElementow.length; i++) {
            if (wynik.isEmpty()) {
                wynik.add(tabliceElementow[i]);
                continue;
            }
            boolean czyDodano = false;
            for (int j = 0; j < wynik.size(); j++) {
                if (tabliceElementow[i] < wynik.get(j)) {
                    wynik.add(j, tabliceElementow[i]);
                    czyDodano = true;
                    break;
                }
            }
            if (!czyDodano) {
                wynik.add(tabliceElementow[i]);
            }
        }
        for (int i=0;i<wynik.size();i++){
            tabliceElementow[i]=wynik.get(i);
        }


        System.out.println();
        return tabliceElementow;
    }
}
