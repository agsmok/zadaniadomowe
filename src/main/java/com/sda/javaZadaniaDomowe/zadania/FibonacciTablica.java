package com.sda.javaZadaniaDomowe.zadania;

import java.util.Scanner;

public class FibonacciTablica {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile elementow ciągu Fibonnaciego chcesz wyświetlić: ");
        String liczbaUzytkownika = scanner.nextLine();
        try {
            Integer liczba = Integer.parseInt(liczbaUzytkownika);
            int[] tablicaFibonacci = new int[liczba];

            if (liczba < 0) System.out.println("Ciąg Fibonacciego zawiera tylko liczby naturalne");
            else {
                int ostatnia = 1;
                int przedostatnia = 1;
                for (int i = 0; i < liczba; i++) {
                    if (liczba < 2) {
                        System.out.println(1);
                        return;
                    }
                    int wynik = ostatnia + przedostatnia;
                    przedostatnia = ostatnia;
                    ostatnia = wynik;
                    System.out.print((tablicaFibonacci[i] = wynik) + " \t");

                }
            }


        } catch (NumberFormatException e) {
            System.out.println("Podano ciąg znaków niebędący liczbą. Program zakończy swoje działanie");
        }


    }


}
