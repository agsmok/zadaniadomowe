package com.sda.javaZadaniaDomowe.zadania;

import java.util.Random;
    /**
     * a) Uzupełnienie tablicy 20 elementowej losowymi liczbami z zakresu <0;100> (Do losowania polecam użyć klasy Random)
     * b) Znalezienie  największego elementu tablicy
     * c) Wyznaczenie średniej arytmetycznej wartości zawartych w tablicy
     * d) Zamiana wartości w tablicy wg następującego schematu (do wykonania w ramach jednej pętli):
     * - jeśli liczba jest < 20 -> pomnóż razy 2
     * - jeśli liczba jest podzielna przez 5 -> Dodaj 2
     * - jeśli liczba jest == 50 -> podnieś do potęgi 2
     * - jeśli liczba jest > 80 -> odejmij od niej wartość pierwszego elementu tablicy
     * - UWAGA - może się okazać że dany element tablicy może spełniać kilka warunków - wtedy należy wykonać każdą czynność, która jest przypisana do tych warunków, które spełnia dana liczba
     */
    public class Zadanie1 {
        public static void main(String[] args) {

            int[] tablica = new int[20];
            uzupelnijLosowymiLiczbami(tablica);
            wyswietlListe(tablica);
            int wynik = znajdzNajwiekszyElement(tablica);
            System.out.println("Największy element tablicy: " + wynik);
            System.out.println("==========================");
            double srednia = sredniaArytmetyczna(tablica);
            System.out.println("Średnia arytmetyczna wartości zawartych w tablicy: " + srednia);
            System.out.println("==========================");
            zmianaWartosciWTablicy(tablica);
            wyswietlListe(tablica);

        }

        private static int[] uzupelnijLosowymiLiczbami(int[] tablica) {
            Random random = new Random();
            for (int i = 0; i < tablica.length; i++) {
                int wylosowanaLiczba = random.nextInt(101);
                for (int it = 0; it < tablica.length; it++) {
                    if (wylosowanaLiczba==tablica[it]) {
                        break;

                    } else {
                        tablica[i] = random.nextInt(101);
                    }
                }
            }
            return tablica;

        }

        private static int znajdzNajwiekszyElement(int[] tablica) {
            int najwiekszyElement = tablica[0];
            for (int i = 0; i < tablica.length; i++) {
                if (najwiekszyElement < tablica[i]) {
                    najwiekszyElement = tablica[i];
                }
            }
            return najwiekszyElement;
        }

        private static double sredniaArytmetyczna(int[] tablica) {
            double sredniaArytmentycz = 0;
            double suma = 0;
            for (int i = 0; i < tablica.length; i++) {
                suma += tablica[i];
                sredniaArytmentycz = suma / tablica.length;

            }
            return sredniaArytmentycz;

        }

        private static int[] zmianaWartosciWTablicy(int[] tablica) {
            for (int i = 0; i < tablica.length; i++) {
                if (tablica[i] < 20) {
                    tablica[i] = 2 * tablica[i];
                }
                if (tablica[i] % 5 == 0) {
                    tablica[i] = 2 + tablica[i];
                }
                if (tablica[i] == 50) {
                    tablica[i] = tablica[i] * tablica[i];
                }
                if (tablica[i] > 80) {
                    tablica[i] = tablica[i] - tablica[0];
                }
            }
            return tablica;
        }

        public static void wyswietlListe(int[] lista) {
            for (int i = 0; i < lista.length; i++) {
                System.out.print(lista[i] + " ");
            }
            System.out.println("\n==========================");
        }
}
