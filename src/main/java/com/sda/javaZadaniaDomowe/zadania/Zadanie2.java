package com.sda.javaZadaniaDomowe.zadania;

import java.util.Scanner;

    /**
     * 2. Należy napisać program, który będzie wczytywał kolejno wartości do tablicy - wartości mają być podawane przez użytkownika - APLIKACJA NIE MOŻE się zamknąć, dopóki użytkownik nie poda 5 poprawnych liczb. W przypadku podania łańcucha nie będącego liczbą należy poinformować użytkownika o błędzie i poprosić o podanie kolejnej liczby.
     */
    public class Zadanie2 {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            int[] tablica = new int[5];

            for (int i = 0; i < tablica.length; i++) {
                String elementTablicy = i + 1 + "";
                System.out.println("Podaj " + elementTablicy + " element tablicy");
                String liczbaUzytkownika = scanner.nextLine();
                try {
                    Integer element = Integer.parseInt(liczbaUzytkownika);
                    tablica[i] = element;
                    if (i == tablica.length - 1) {
                        for (int item : tablica) {
                            System.out.print(item + " ");
                        }
                    }
                } catch (NumberFormatException e) {
                    System.out.println("BŁĄD!!! Podałeś łancuch znaków nie będący liczbą, podaj liczbę");
                    i--;
                }
            }
        }
    }
