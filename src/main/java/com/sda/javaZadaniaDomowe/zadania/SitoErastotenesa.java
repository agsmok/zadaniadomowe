package com.sda.javaZadaniaDomowe.zadania;
/**
 * 4. Program do obliczania sita Erastotenesa. Użytkownik podaje liczbę naturalną  k> 1, a program ze zbioru (1;k> ma wyszukać wszystkie liczby pierwsze.
 */
import java.util.Scanner;

public class SitoErastotenesa {
    public static void main(String[] args) {
        System.out.println("Program do obliczania sita Erastotenesa");

        startProgram();

    }

    private static void startProgram() {
        int zmienna = 0;
        try {
            zmienna = pobierzDane();
            liczbaMniejszaOdJeden(zmienna);
        } catch (NumberFormatException e) {
            System.out.println("Błąd! Podałeś ciąg znaków niebędący liczbą");
            startProgram();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            startProgram();
        }
        obliczSito(zmienna);
    }


    private static int pobierzDane() throws NumberFormatException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę większą od 1: ");
        String ostatniaLiczba = scanner.next();

        return Integer.parseInt(ostatniaLiczba);
    }

    private static void liczbaMniejszaOdJeden(int zmienna) throws Exception {
        if (zmienna <= 1) {
            throw new Exception("Błąd! Podana liczba nie jest większa od 1");
        }
    }

    private static void obliczSito(int liczba) {

        int pierwsze[] = new int[liczba];

        for (int i = 0; i < liczba; i++) {
            pierwsze[i] = i + 1;
            pierwsze[0] = 0;
            pierwsze[1] = 0;
        }

        pierwsze = usunWielokrotnosci(liczba, pierwsze);

        wydrukujWynik(pierwsze);
        System.exit(0);
    }

    private static void wydrukujWynik(int[] pierwsze) {
        for (int i = 0; i < pierwsze.length; i++) {
            if (pierwsze[i] != 0) {
                System.out.println(i);
            }
        }
    }

    private static int[] usunWielokrotnosci(int liczba, int[] pierwsze) {
        for (int i = 2; i < liczba; i++) {
            if (pierwsze[i] != 0) {
                for (int j = i + i; j < liczba; j = j + i) {
                    pierwsze[j] = 0;
                }
            }
        }
        return pierwsze;
    }
}
