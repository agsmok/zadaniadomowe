package com.sda.javaZadaniaDomowe.pracownik;

/**
 * 1 Utórz klasę pracownik która posiada pola:
 * imie,nazwisko,stanowisko,pensja
 * konstruktor, gettery, settery, to string
 * 2 Weź z listy osób osoby w wieku 21-40 lat a następnie zmapuj każdy z nich na obiekt typu pracownik
 * zamiast persona musumy mieć obiety pracownik
 * 3 Wyswietl wyniki
 * 4 Wykonujemy ponownie pkt 2 a następnie z tych obiektów które mamy wybieramy tylko te osoby których imie zaczyna się na J
 * Sumujemy zarobki tych osób
 */
public class Pracownik {
    String imie;
    String nazwisko;
    String stanowisko;
    String pensja;

    public Pracownik() {
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getStanowisko() {
        return stanowisko;
    }

    public void setStanowisko(String stanowisko) {
        this.stanowisko = stanowisko;
    }

    public String getPensja() {
        return pensja;
    }

    public void setPensja(String pensja) {
        this.pensja = pensja;
    }

    @Override
    public String toString() {
        return "Pracownik{" + "imie='" + imie + '\'' + ", nazwisko='" + nazwisko + '\'' + ", stanowisko='" + stanowisko + '\'' + ", pensja='" + pensja + '\'' + '}';
    }
}
